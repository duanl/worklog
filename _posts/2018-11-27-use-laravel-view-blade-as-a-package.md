---
layout: post
title: Use laravel view(blade) as a package
date: 2018-11-27 22:00 +0000
---


## Three blade packages
* Laravel-Blade [Source code](https://github.com/PhiloNL/Laravel-Blade/blob/master/src/Blade.php) [Documents](https://github.com/PhiloNL/Laravel-Blade) 
* laraport/blade [Source code](https://github.com/laraport/blade/blob/master/src/Blade.php) [Documents](https://github.com/laraport/blade) 
* jenssegers/blade [Source code](https://github.com/jenssegers/blade/blob/master/src/Blade.php) [Documents](https://github.com/jenssegers/blade) 


---