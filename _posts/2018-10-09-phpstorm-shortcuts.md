---
layout: post
title: PHPstorm shortcuts
date: 2018-10-09 04:25 +0000
---
* `⌘`: Command key
* `⌥`: Option key (ALT in windows)
* `^`: Control key
* `⇧`: Shift key

Description | Shortcut
------------ | ------------- 
Search everywhere | Double `⇧` 
Recent file | `⌘E` 
Duplicate line | `⌘D`
Move line up | `⌘↑` 
Move line down | `⌘↓` 
Find Action | `⌘⇧A`
Refactor this | `^T`


* [Mastering PhpStorm keyboard shortcuts](https://www.jetbrains.com/help/phpstorm/mastering-keyboard-shortcuts.html)

--- 