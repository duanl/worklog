---
layout: post
title: PHP command line
date: 2018-10-26 00:25 +0000
---


* `php -i`: like phpinfo(); you can use `php -i | less` to show detail one by one.
* `php -f sample.php`: run php file in cli.
* `php -r`: run php code in cli, like `php -r "print_r('hi');"`
* `php -a`: Interactive shell for PHP [^1]

[^1]: [How to Use and Execute PHP Codes in Linux Command Line](https://www.tecmint.com/run-php-codes-from-linux-commandline/)


---




